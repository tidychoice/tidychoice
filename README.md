We were founded in 2015 to help Londoners find trusted, reliable cleaners easily online. With just a few clicks you can browse, choose and book a fully vetted cleaner directly. You hire your preferred cleaner and we handle payments and scheduling effortlessly through our platform. As well as helping you find a great local cleaner, we help cleaners find work when and where it suits them. We have grown quickly to become Londons leading domestic cleaning service. We have thousands of bookings providing hundreds of cleaners with fairly paid work.

Website: https://www.tidychoice.com
